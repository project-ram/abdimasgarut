if( $("#estate-map").length ) 
{
	google.maps.event.addDomListener(window, 'load', init);
	
	function init() 
	{
		var lat = $('#estate-map').data('lat');
		var long = $('#estate-map').data('long');
		var title = $('#estate-map').data('title');
		var address = $('#estate-map').data('address');
		var img = $('#estate-map').data('img');
		var url = $('#estate-map').data('url');
		
		if(lat != '' && long != '')
		{
			mapInitInfobox(lat,long,'estate-map',[title,address,img,url],'assets/frontend/images/pin-house.png', true);
			streetViewInit(lat,long,'estate-street-view');
		}
		else
			mapInit(-7.190134,107.889733,'estate-map','', true);
	}
}

if( $(".grid-offer-map").length ) 
{
	google.maps.event.addDomListener(window, 'load', init);
	
	function init() 
	{
		$('.grid-offer-map').each(function(){
			var lat = $(this).data('lat');
			var long = $(this).data('long');
			var id = $(this).attr('id'); console.log(id);
			
			if(lat != '' && long != '')
			{
				mapInit(lat,long,id,'assets/frontend/images/pin-house.png', false);
			}
			else
				mapInit(-7.190134,107.889733,id,'', false);		
		});
	}
}

