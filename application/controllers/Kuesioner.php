<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *
 * Controller Kuesioner
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Kuesioner extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->view('kuesioner/kuesioner_embed');
  }
  public function csiForm()
  {
    $this->load->view('kuesioner/kuesioner');
  }
  public function submitForm(){
    // print_r($_POST);
    $this->load->view('kuesioner/success');
  }
}


/* End of file Kuesioner.php */
/* Location: ./application/controllers/Kuesioner.php */