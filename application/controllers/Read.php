<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Read extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Posts_model', 'pm', TRUE);
    $this->load->model('Ms_tags_model', '', TRUE);
  }

  public function index()
  {
    $url = $this->uri->segment(3);
    $explode_url = explode('-',$url);
    $get_id = end($explode_url);
    $data['data'] = $this->pm->getbyid($get_id)->row();
    $data['tags'] = $this->Ms_tags_model->get_tag_bypost($get_id)->result();
    $this->load->view('read/post',$data);
  }

}


/* End of file Read.php */
/* Location: ./application/controllers/Read.php */