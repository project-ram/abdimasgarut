<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Menu extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Ms_category_model','cat');
    $this->load->model('Posts_model','pm');

  }

  public function type()
  {
    // 
    $url = $this->uri->segment(3);
    
    switch($url){
      case 'wisata-halal':
          $data['cover_title'] ='Wisata Halal';
          $data['cover_img'] = 'assets/frontend_global/img/cover/wisata-halal.jpg';
          $this->load->view('category/cat_wisata_halal',$data);
        break;
      case 'destinasi-wisata':
          $data['cover_title'] ='Destinasi Wisata';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat($url)->result();
          $this->load->view('category/cat_destinasi_wisata',$data);
        break;
      case 'event':
          $data['cover_title'] ='Event';
          $data['cover_img'] = 'assets/frontend_global/img/cover/eventpwa.jpg';
          $data['post'] = $this->pm->get_post_cat($url)->result();
          $this->load->view('category/cat_event',$data);
        break;
      case 'kuliner':
          $data['cover_title'] ='Kuliner';
          $data['cover_img'] = 'assets/frontend_global/img/cover/kulinerx.jpg';
          $data['post'] = $this->pm->get_post_cat($url)->result();
          $this->load->view('category/cat_kuliner',$data);
        break;
      case 'promo':
          $data['cover_title'] ='Promo';
          $data['cover_img'] = 'assets/frontend_global/img/cover/promo.jpg';
          $data['post'] = $this->pm->get_post_cat($url)->result();
          $this->load->view('category/cat_promo',$data);
        break;
      default:
        $this->load->view('err404/index');

    }
  }

}


/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */