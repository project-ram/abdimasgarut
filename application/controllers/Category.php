<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Category extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Ms_category_model','cat');
    $this->load->model('Posts_model','pm');

  }

  public function category()
  {
    // 
    $url = $this->uri->segment(3);
    switch($url){
      case 'wisata-religi':
          $data['cover_title'] ='Wisata Religi';
          $data['cover_img'] = 'assets/frontend_global/img/cover/wisata-halal.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('wisata-halal',$data['cover_title'])->result();
          $this->load->view('category/wisata_halal/detail',$data);
        break;
      case 'masjid':
          $data['cover_title'] ='Masjid';
          $data['cover_img'] = 'assets/frontend_global/img/wisata_halal/cover/masjid.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('wisata-halal',$data['cover_title'])->result();
          $this->load->view('category/wisata_halal/detail',$data);
        break;
      case 'pesantren':
          $data['cover_title'] ='Pesantren';
          $data['cover_img'] = 'assets/frontend_global/img/wisata_halal/cover/pesantren.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('wisata-halal',$data['cover_title'])->result();
          $this->load->view('category/wisata_halal/detail',$data);
        break;
      case 'tempat-penghafal-alquran':
          $data['cover_title'] ='Tempat Penghafal Alquran';
          $data['cover_img'] = 'assets/frontend_global/img/wisata_halal/cover/tempat-penghafal-alquran.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('wisata-halal',$data['cover_title'])->result();
          $this->load->view('category/wisata_halal/detail',$data);
        break;
      case 'hotel-syariah':
          $data['cover_title'] ='Hotel Syariah';
          $data['cover_img'] = 'assets/frontend_global/img/wisata_halal/cover/hotel-syariah.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('wisata-halal',  $data['cover_title'])->result();
          $this->load->view('category/wisata_halal/detail',$data);
        break;
      case 'kspk-garut-selatan':
          $data['cover_title'] ='KSPK Garut Selatan';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'kspk-garut-tengah':
          $data['cover_title'] ='KSPK Garut Tengah';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'kspk-garut-utara':
          $data['cover_title'] ='KSPK Garut Utara';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'kspk-perkotaan-garut':
          $data['cover_title'] ='KSPK Perkotaan Garut';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'wisata-air-panas':
          $data['cover_title'] ='Wisata Air Panas';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'wisata-alam':
          $data['cover_title'] ='Wisata Alam';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'wisata-buatan':
          $data['cover_title'] ='Wisata Buatan';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'wisata-belanja':
          $data['cover_title'] ='Wisata Belanja';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'wisata-edutainment':
          $data['cover_title'] ='Wisata Edutainment';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'wisata-heritage':
          $data['cover_title'] ='Wisata Heritage';
          $data['cover_img'] = 'assets/frontend_global/img/cover/destinasi-wisata-pwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('destinasi-wisata',$data['cover_title'])->result();
          $this->load->view('category/destinasi/detail',$data);
        break;
      case 'hotel':
          $data['cover_title'] ='Hotel';
          $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('hotel',$data['cover_title'])->result();
          $this->load->view('category/hotel/detail',$data);
        break;
      case 'penginapan':
          $data['cover_title'] ='Penginapan';
          $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('hotel',$data['cover_title'])->result();
          $this->load->view('category/hotel/detail',$data);
        break;
      case 'resort':
          $data['cover_title'] ='Resort';
          $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('hotel',$data['cover_title'])->result();
          $this->load->view('category/hotel/detail',$data);
        break;
      case 'cerita-daerah':
        $data['cover_title'] ='Cerita Daerah';
        $data['cover_img'] = 'assets/frontend_global/img/wisata_halal/cover/cerita-daerah.jpg';
        $data['post'] = $this->pm->get_post_cat_submenu('budaya',$data['cover_title'])->result();
        $this->load->view('category/budaya/detail',$data);
      break;
      case 'atraksi':
          $data['cover_title'] ='Atraksi';
          // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('budaya',$data['cover_title'])->result();
          $this->load->view('category/budaya/detail',$data);
        break;
      case 'asal-usul':
          $data['cover_title'] ='Asal Usul';
          // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('budaya',$data['cover_title'])->result();
          $this->load->view('category/budaya/detail',$data);
        break;
      case 'kerajinan':
          $data['cover_title'] ='Kerajinan';
          // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('budaya',$data['cover_title'])->result();
          $this->load->view('category/budaya/detail',$data);
        break;
      case 'kesenian':
          $data['cover_title'] ='Kesenian';
          // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('budaya',$data['cover_title'])->result();
          $this->load->view('category/budaya/detail',$data);
        break;
      case 'lagu-daerah':
          $data['cover_title'] ='Lagu Daerah';
          // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('budaya',$data['cover_title'])->result();
          $this->load->view('category/budaya/detail',$data);
        break;
      case 'event-festival-nasional':
          $data['cover_title'] ='Event / Festival Nasional';
          // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('event',$data['cover_title'])->result();
          $this->load->view('category/event/detail',$data);
        break;
      case 'event-festival-internasional':
          $data['cover_title'] ='Event / Festival Internasional';
          // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
          $data['post'] = $this->pm->get_post_cat_submenu('event',$data['cover_title'])->result();
          $this->load->view('category/event/detail',$data);
        break;
      case 'berita':
        $data['cover_title'] ='Berita';
        // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
        $data['post'] = $this->pm->get_post_cat_submenu('informasi',$data['cover_title'])->result();
        $this->load->view('category/info/detail',$data);
      break;
      case 'majalah':
        $data['cover_title'] ='Majalah';
        // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
        $data['post'] = $this->pm->get_post_cat_submenu('informasi',$data['cover_title'])->result();
        $this->load->view('category/info/detail',$data);
      break;
      case 'video':
        $data['cover_title'] ='Video';
        // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
        $data['post'] = $this->pm->get_post_cat_submenu('informasi',$data['cover_title'])->result();
        $this->load->view('category/info/detail',$data);
      break;
      case 'tentang':
        $data['cover_title'] ='Tentang';
        // $data['cover_img'] = 'assets/frontend_global/img/cover/hotelpwa.jpg';
        $data['post'] = $this->pm->get_post_cat_submenu('informasi',$data['cover_title'])->result();
        $this->load->view('category/info/detail',$data);
      break;
      case 'promo':
        $data['cover_title'] ='Promo';
        $data['cover_img'] = 'assets/frontend_global/img/cover/promo.jpg';
        $data['post'] = $this->pm->get_post_cat('promo')->result();
        $this->load->view('category/cat_promo',$data);
      break;
      case 'kuliner':
        $data['cover_title'] ='Kuliner';
        $data['cover_img'] = 'assets/frontend_global/img/cover/kulinerx.jpg';
        $data['post'] = $this->pm->get_post_cat('kuliner')->result();
        $this->load->view('category/cat_promo',$data);
      break;
      default:
        $this->load->view('err404/index');

    }
  }

}


/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */