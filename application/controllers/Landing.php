<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Posts_model','landing');

  }

  public function index()
  {
    // 
    $data['slider'] = $this->landing->get_slider()->result();
    $data['popular'] = $this->landing->get_popular()->result();
    $data['promo'] =  $this->landing->get_post_promo_landing()->result();
    $this->load->view('landing/landing',$data);
  }

}


/* End of file Landing.php */
/* Location: ./application/controllers/Landing.php */