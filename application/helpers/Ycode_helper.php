<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * Helpers Ycode_helper
 *
 * This Helpers for ...
 * 
 */

// ------------------------------------------------------------------------

function y_ellipsis($str, $length)
{
	$str = strip_tags(trim($str));
	
	// If the text is already shorter than the max length, then just return unedited text.
	if (strlen($str) <= $length) {
		return $str;
	}
	
	// Find the last space (between words we're assuming) after the max length.
	$last_space = strrpos(substr($str, 0, $length), ' ');
	// Trim
	$trimmed_text = substr($str, 0, $last_space);
	// Add ellipsis.
	$trimmed_text .= ' ...';
	
	return ucfirst($trimmed_text);
}
function y_lang_excerpt($excerpt_id, $content_id, $excerpt_en, $content_en, $length)
{
	echo y_ellipsis($content_id, $length);
}
function y_lang_text($text)
{
	$ci = &get_instance();
	if($ci->lang->line($text))
		return $ci->lang->line($text);
	else
		return $text;
}
// ------------------------------------------------------------------------

/* End of file Ycode_helper.php */
/* Location: ./application/helpers/Ycode_helper.php */