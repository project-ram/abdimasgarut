<?php
defined('BASEPATH') or exit('No direct script access allowed');


// ------------------------------------------------------------------------
  function date_only($post_date){
    $date = date_create($post_date);
    return date_format($date,"d/m/Y");
  }
// ------------------------------------------------------------------------

/* End of file Date_helper.php */
/* Location: ./application/helpers/Date_helper.php */