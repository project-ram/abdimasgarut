<div class="menu-wrapper">
    <div class="menu-header no-border">
        <a href="#"><img src="<?php echo base_url(); ?>assets/images/logo.png" class="my-4 w-2/6"></a>
        <a href="#" class="close-menu"><i class="fa fa-times color-red2-dark"></i></a>
    </div>

    <div class="menu-list">
        <a href="#" data-dropdown="wisata-halal" class="dropdown-style-1 menu-item">
            <i class="demo-icon icon-halal font-18 dropdown text_active"></i>
            <span class="dropdown text_active">Wisata Halal</span>
            <i class="fa dropdown-icon fa-angle-right color-theme"></i>
        </a>
        <ul class="ml-20 dropdown-content bottom-10" id="wisata-halal">
            <li><a href="<?php echo base_url();?>id/category/wisata-religi">Wisata Religi</a></li>
            <li><a href="<?php echo base_url();?>id/category/masjid">Masjid</a></li>
            <li><a href="<?php echo base_url();?>id/category/pesantren">Pesantren</a></li>
            <li><a href="<?php echo base_url();?>id/category/tempat-penghafal-alquran">Tempat Penghafal Al-Quran</a></li>
            <li><a href="<?php echo base_url();?>id/category/cerita-daerah">Cerita Daerah</a></li>
            <li><a href="<?php echo base_url();?>id/category/hotel-syariah">Hotel Syariah</a></li>
        </ul>
        <a href="#" data-dropdown="destinasi-wisata" class="dropdown-style-1 menu-item">
            <i class="demo-icon icon-destination font-18 dropdown text_active"></i>
            <span class="dropdown text_active">Destinasi Wisata</span>
            <i class="fa dropdown-icon fa-angle-right color-theme"></i>
        </a>
        <ul class="ml-20 hidden" id="destinasi-wisata">
            <li><a href="<?php echo base_url();?>id/category/kspk-garut-selatan">KSPK Garut Selatan</a></li>
            <li><a href="<?php echo base_url();?>id/category/kspk-garut-tengah">KSPK Garut Tengah</a></li>
            <li><a href="<?php echo base_url();?>id/category/kspk-garut-utara">KSPK Garut Utara</a></li>
            <li><a href="<?php echo base_url();?>id/category/kspk-perkotaan-garut">KSPK Perkotaan Garut</a></li>
            <li><a href="<?php echo base_url();?>id/category/wisata-air-panas">Wisata Air Panas</a></li>
            <li><a href="<?php echo base_url();?>id/category/wisata-alam">Wisata Alam</a></li>
            <li><a href="<?php echo base_url();?>id/category/wisata-buatan">Wisata Buatan</a></li>
            <li><a href="<?php echo base_url();?>id/category/wisata-belanja">Wisata Belanja</a></li>
            <li><a href="<?php echo base_url();?>id/category/wisata-edutainment">Wisata Edutainment</a></li>
            <li><a href="<?php echo base_url();?>id/category/wisata-heritage">Wisata Heritage</a></li>
        </ul>
        <a class="menu-item" href="<?php echo base_url();?>id/category/kuliner">
            <i class="demo-icon icon-culinery font-18"></i>
            <span>Kuliner</span>
        </a>
        <a href="#" data-dropdown="hotel" class="dropdown-style-1 menu-item">
            <i class="demo-icon icon-hotel font-18 dropdown text_active"></i>
            <span class="dropdown text_active">Hotel</span>
            <i class="fa dropdown-icon fa-angle-right color-theme"></i>
        </a>
        <ul class="ml-20 hidden" id="hotel">
            <li><a href="<?php echo base_url();?>id/category/hotel">Hotel</a></li>
            <li><a href="<?php echo base_url();?>id/category/penginapan">Penginapan</a></li>
            <li><a href="<?php echo base_url();?>id/category/resort">Resort</a></li>
        </ul>
        <a href="#" data-dropdown="budaya" class="dropdown-style-1 menu-item">
            <i class="demo-icon icon-culture font-18 dropdown text_active"></i>
            <span class="dropdown text_active">Budaya</span>
            <i class="fa dropdown-icon fa-angle-right color-theme"></i>
        </a>
        <ul class="ml-20 hidden" id="budaya">
            <li>
                <a href="<?php echo base_url();?>id/category/atraksi">Atraksi</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>id/category/cerita-daerah">Cerita Daerah</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>id/category/asal-usul">Asal Usul</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>id/category/kerajinan">Kerajinan</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>id/category/kesenian">Kesenian</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>id/category/lagu-daerah">Lagu Daerah</a>
            </li>
        </ul>
        <a class="menu-item" href="<?php echo base_url();?>id/category/promo">
            <i class="demo-icon icon-promo font-18"></i>
            <span>Promo</span>
        </a>
        <a href="#" data-dropdown="event" class="dropdown-style-1 menu-item">
            <i class="demo-icon icon-event font-18 dropdown text_active"></i>
            <span class="dropdown text_active"">Event</span>
            <i class="fa dropdown-icon fa-angle-right color-theme"></i>
        </a>
        <ul class="ml-20 hidden" id="event">
            <li><a href="<?php echo base_url();?>id/category/event-festival-nasional">Event / Festival Nasional</a></li>
            <li><a href="<?php echo base_url();?>id/category/event-festival-internasional"">Event / Festival Internasional</a></li>
        </ul>
        <a href="#" data-dropdown="info" class="dropdown-style-1 menu-item">
            <i class="demo-icon icon-info font-18 dropdown text_active"></i>
            <span class="dropdown text_active">Informasi</span>
            <i class="fa dropdown-icon fa-angle-right color-theme"></i>
        </a>
        <ul class="ml-20 hidden" id="info">
            <li><a href="<?php echo base_url();?>id/category/berita">Berita</a></li>
            <li><a href="<?php echo base_url();?>id/category/majalah">Majalah</a></li>
            <li><a href="<?php echo base_url();?>id/category/video">Video</a></li>
            <li><a href="<?php echo base_url();?>id/category/tentang">Tentang</a></li>
        </ul>
        <a class="menu-item" href="<?php echo base_url();?>kuesioner">
            <i class="fas fa-clipboard-check"></i>
            <span>Kuesioner</span>
        </a>
    </div>

    <!-- <p class="menu-divider bottom-30">CONTACT US TODAY</p>

    <div class="menu-list">
        <a class="menu-item" href="#"><i class="fa fa-phone"></i><span>Call</span><i class="fa fa-angle-right"></i></a>
        <a class="menu-item" href="#"><i class="fa fa-envelope"></i><span>Email</span><i class="fa fa-angle-right"></i></a>
        <a class="menu-item" href="#"><i class="fa fa-comment"></i><span>Message</span><i class="fa fa-angle-right"></i></a>
    </div> -->

    <!-- <p class="menu-divider">COPYRIGHT ENABLED. ALL RIGHTS RESERVED</p> -->
    <p class="menu-divider font-10 mt-4"> &copy; Copyright <span class="copyright-year"></span> Portal Pesona Garut. <br> All Rights Reserved
    &bull; Supported by Telkom University</p>
</div>