<div class="footer-title">
    <a href="#">
        <img src="<?php echo base_url();?>assets/frontend_global/img/logo-portal-pesona-garut-footer.png" class="w-3/6">
    </a>
</div>
<p class="color-theme font-12 px-8 my-3">
    Jl. Otto Iskandardinata No.278A, Garut 44151, Jawa Barat, Indonesia
</p>
<p class="color-theme font-10 px-8 my-3">
   <a href="mailto:info@visitgarut.garutkab.go.id">info@visitgarut.garutkab.go.id</a> | +62-262-233-529
</p>
<div class="footer-socials">
    <a  href="https://www.instagram.com/disparbud_garut/" target="_blank"><i class="fab fa-instagram bg-instagram"></i></a>
    <a href="https://www.youtube.com/channel/UCDq-CiSYkI8p1eHjFGzvXTQ" target="_blank"><i class="fab fa-youtube-square bg-youtube"></i></a>
    <a href="https://www.facebook.com/disparbud.garut" target="_blank"><i class="fab fa-facebook-f bg-facebook"></i></a>
    <a href="tel:+62-262-233-529"><i class="fa fa-phone bg-green1-dark"></i></a>
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up bg-highlight color-white"></i></a>
</div>
<p class="text-black font-10 mt-4"> &copy; Copyright <span class="copyright-year"></span> Portal Pesona Garut. All Rights Reserved</p>
<p class="text-black font-10 mt-2">Supported by Telkom University</p>


<script>
$(document).ready(function(){      
    'use strict'	
    //Back to Top
    $('.back-to-top').on( "click", function(e){
        e.preventDefault();
        $('html, body, .page-content').animate({
            scrollTop: 0
        }, 350);
        return false;
    });
    
    //Copyright Year 
    var copyrightYear = $('.copyright-year ,#copyright-year');
    var dteNow = new Date(); var intYear = dteNow.getFullYear();
    copyrightYear.html(intYear);
});
</script>