<?php $this->load->view('header/index');?>
<div class="preloader"><div><!--Preloader Generated Here--></div></div>
<div id="page">

	<div class="header header-fixed header-logo-left">
        <a href="<?php echo base_url(); ?>" class="header-title">
            <img src="<?php echo base_url(); ?>assets/images/logo.png" class="my-4 w-2/6">
        </a>
		<!-- <a href="#" class="header-icon header-icon-2" data-menu="menu-colors"><i class="fas fa-palette"></i></a> -->
        <a href="#" class="header-icon header-icon-1" data-menu="menu-contact"><i class="fas fa-bars"></i></a>
	</div>

    <div class="page-content header-clear">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/news.css">

		<div class="single-slider owl-carousel owl-no-dots bottom-30">
			<?php foreach ($slider as $item): ?>
				<a href="<?php echo base_url().'read/index/'.$item->post_url_id;?>-<?php echo $item->post_id;?>" class="news-slide-1">
					<!-- <img src="<?php echo base_url(); ?>assets/images/pictures/18m.jpg" class="responsive-image" alt=""> -->
					<img src="<?php echo base_url(); ?><?php echo $item->post_img; ?>" class="responsive-image" alt="">
					<strong class="font-16 font-black">
						<?php echo $item->post_title_id; ?>
						<div class="font-14 mt-2 font-light">
							<?php y_lang_excerpt($item->post_excerpt_id, $item->post_content_id, $item->post_excerpt_en, $item->post_content_id, 150);?>
						</div>
					</strong>
					<em class="bg-highlight"><?php echo $item->cat_name_id;?></em>
					<span class="bg-gradient"></span>
				</a>
			<?php endforeach;?>
		</div>
		<div class="mx-4 my-16"> 
			<h1 class="text-3xl text-primary text-center font-semibold">GARUT : THE PARADISE PREANGER</h1>
			<p class="my-3 text-black text-center font-14">	
			Selamat datang di Garut. Dimana keindahan alam priangan berpadu-padan dengan kekayaan produk budaya masyarakatnya. Garis pantai-nya yang terpandang di Jawa Barat juga melengkapi rangkaian pegunungannya. Santapan lezat khasnya hanya bisa dikalahkan oleh ramah tamah warganya. Selamat datang di garut, surga tanah priangan. Garut merupakan salah satu kota yang mempunyai destinasi wisata terbaik. Garut merupakan Swiss of Java.
					
			</p>
		</div>
		<div class="my-12 text-center px-5">
			<img src="<?php echo base_url() ?>assets/frontend_global/img/icon_homepage/wisata-religi-125x125.png" width="125" height="125">
			<div class="text-primary font-16 my-4"><?php echo y_lang_text('wisata_religi');?></div>
			<?php echo y_lang_text('wisata_religi_text');?>
		</div>		
		<div class="my-12 text-center px-5">
			<img src="<?php echo base_url() ?>assets/frontend_global/img/icon_homepage/wisata-keluarga-125x125.png" width="125" height="125">
			<div class="text-primary font-16 my-4"><?php echo y_lang_text('wisata_keluarga');?></div>
			<?php echo y_lang_text('wisata_keluarga_text');?>
		</div>		
		<div class="my-12 text-center px-5">
			<img src="<?php echo base_url() ?>assets/frontend_global/img/icon_homepage/jajanan-tradisional-125x125.png" width="125" height="125">
			<div class="text-primary font-16 my-4"><?php echo y_lang_text('jajanan_tradisional');?></div>
			<?php echo y_lang_text('jajanan_tradisional_text');?>
		</div>		
		<div class="my-12 text-center px-5">
			<img src="<?php echo base_url() ?>assets/frontend_global/img/icon_homepage/belanja-125x125.png" width="125" height="125">
			<div class="text-primary font-16 my-4"><?php echo y_lang_text('belanja');?></div>
			<?php echo y_lang_text('belanja_text');?>
		</div>		
		<div class="my-12 text-center px-5">
			<img src="<?php echo base_url() ?>assets/frontend_global/img/icon_homepage/hiburan-125x125.png" width="125" height="125">
			<div class="text-primary font-16 my-4"><?php echo y_lang_text('hiburan');?></div>
			<?php echo y_lang_text('hiburan_text');?>
		</div>		
		<div class="my-12 text-center px-5">
			<img src="<?php echo base_url() ?>assets/frontend_global/img/icon_homepage/petualangan-125x125.png" width="125" height="125">
			<div class="text-primary font-16 my-4"><?php echo y_lang_text('petualangan');?></div>
			<?php echo y_lang_text('petualangan_text');?>
		</div>		
		<div class="my-12 text-center px-5">
			<img src="<?php echo base_url() ?>assets/frontend_global/img/icon_homepage/warisan-budaya-125x125.png" width="125" height="125">
			<div class="text-primary font-16 my-4"><?php echo y_lang_text('warisan_budaya');?></div>
			<?php echo y_lang_text('warisan_budaya_text');?>
		</div>		
		<div class="my-12 text-center px-5">
			<img src="<?php echo base_url() ?>assets/frontend_global/img/icon_homepage/relaksasi-125x125.png" width="125" height="125">
			<div class="text-primary font-16 my-4"><?php echo y_lang_text('relaksasi');?></div>
			<?php echo y_lang_text('relaksasi_text');?>
		</div>

		<!-- POPOULAR -->
	<div class="mx-4 my-16"> 
		<h1 class="text-3xl text-primary text-center font-semibold">TERPOPULER SAAT INI</h1>
	</div>
	<div class="content bottom-0">
		<!-- First Column -->
		<?php
			// $len = sizeof($popular);
			foreach($popular as $pop):
		?>
		<?php 
			// if($len % 2 != 0):
		?>
			<!-- <div class="one-half">
				<div class="news-col-item">
					<a href="<?php echo base_url().'read/index/'.$item->post_url_id;?>-<?php echo $item->post_id;?>">
						<img class="preload-image shadow-large" src="<?php echo base_url(); ?><?php echo $pop->post_img; ?>" alt="img">
						<strong class="font-14 mb-4"><?php echo $pop->post_title_id;?></strong>
						<?php y_lang_excerpt($pop->post_excerpt_id, $pop->post_content_id, $pop->post_excerpt_en, $pop->post_content_id, 150);?>
					</a>
					<span class="font-12"><i class="fas fa-clock"></i><?php echo date_only($pop->post_date); ?></span>
				</div>
			</div> -->
		<?php
			// endif;
		?>
			<!-- Second Column -->
		<?php 
			// if($len % 2 == 0):
		?>
			<div class="one-half last-column">
				<div class="news-col-item">
					<a href="<?php echo base_url().'read/index/'.$pop->post_url_id;?>-<?php echo $pop->post_id;?>">
						<img class="preload-image shadow-large" src="<?php echo base_url(); ?><?php echo $pop->post_img; ?>" alt="img">
						<strong class="font-14 mb-4"><?php echo $pop->post_title_id;?></strong>
						<?php y_lang_excerpt($pop->post_excerpt_id, $pop->post_content_id, $pop->post_excerpt_en, $pop->post_content_id, 150);?>
					</a>
					<span class="font-12"><i class="fas fa-clock"></i><?php echo date_only($pop->post_date); ?></span>
				</div>
			</div>
		<?php
			// endif;
		?>
		<?php endforeach; ?>
			<div class="clear"></div>

            <div class="divider top-30"></div>
			<!-- PROMO -->
			<div class="mx-4 my-16"> 
				<h1 class="text-3xl text-primary text-center font-semibold">LATEST PROMO</h1>
			</div>
			<?php
				// print_r($promo);
				foreach($promo as $pro):
			?>
			<div class="news-list-item">
				<a href="<?php echo base_url().'read/index/'.$pro->post_url_id;?>-<?php echo $pro->post_id;?>">
					<img class="preload-image shadow-large round-small" src="<?php echo base_url(); ?>assets/images/empty.png" data-src="<?php echo base_url(); ?><?php echo $pro->post_img;?>" alt="img">
					<strong><?php echo $pro->post_title_id;?></strong>
				</a>
				<div class="pl-22">
					<?php y_lang_excerpt($pro->post_excerpt_id, $pro->post_content_id, $pro->post_excerpt_en, $pro->post_content_id, 150);?>
				</div>
				<span><i class="fas fa-clock"></i><?php echo date_format(date_create($pro->post_date),"d/m/Y");?></span>
			</div>
			<?php
				endforeach;
			?>
			<!-- <div class="news-list-item">
				<a href="#">
					<img class="preload-image shadow-large round-small" src="<?php echo base_url(); ?>assets/images/empty.png" data-src="<?php echo base_url(); ?>assets/images/pictures/5s.jpg" alt="img">
					<strong>Bane's favorte fruit are market fresh strawberries</strong>
				</a>
				<span><i class="fas fa-clock"></i>30 Dec 2019 <a href="#" class="color-highlight">Nutrition</a></span>
			</div>
			<div class="news-list-item">
				<a href="#">
					<img class="preload-image shadow-large round-small" src="<?php echo base_url(); ?>assets/images/empty.png" data-src="<?php echo base_url(); ?>assets/images/pictures/7s.jpg" alt="img">
					<strong>Coffee now being used to power cars. Wait, what?!</strong>
				</a>
				<span><i class="fas fa-clock"></i>30 Dec 2019 <a href="#" class="color-highlight">Technology</a></span>
			</div>
			<div class="news-list-item no-border bottom-0">
				<a href="#">
					<img class="preload-image shadow-large round-small" src="<?php echo base_url(); ?>assets/images/empty.png" data-src="<?php echo base_url(); ?>assets/images/pictures/5s.jpg" alt="img">
					<strong>Coffee now being used to power cars. Wait, what?!</strong>
				</a>
				<span><i class="fas fa-clock"></i>30 Dec 2019 <a href="#" class="color-highlight">Technology</a></span>
			</div> -->
			<!-- END OF PROMO -->
			<!-- SUPPORT BY -->
			<div class="mb-20 mt-5">
				<h1 class="text-2xl text-primary text-center font-semibold mb-8">Support By</h1>
				<div class="flex">
					<div class="flex-initial">
						<img src="<?php echo base_url();?>assets/frontend/images/logo/11.png" class="w-32">
					</div>
					<div class="flex-initial">
						<img src="<?php echo base_url();?>assets/frontend/images/logo/2.png" class="w-32">
					</div>
					<div class="flex-initial">
						<img src="<?php echo base_url();?>assets/frontend/images/logo/1.png" class="w-32">
					</div>
				</div>
				<div class="flex mx-auto mt-8">
					<div class="flex-initial mx-auto">
						<img src="<?php echo base_url();?>assets/frontend/images/logo/3.png" class="w-32">
					</div>
					<div class="flex-initial mx-auto">
						<img src="<?php echo base_url();?>assets/frontend/images/logo/0.png" class="w-32">
					</div>
				</div>
			</div>
	</div>
        <!-- <div class="divider divider-margins"></div> -->
        <div class="footer text-center">
			<?php
				include('footer.php');
			?>
		</div>
    </div>
	<?php
		$this->load->view('footer_menu')
	?>
	
    <div id="menu-contact"
         class="menu-box"
         data-menu-type="menu-box-right"
         data-menu-width="280"
         data-menu-effect="menu-reveal">
		 <?php $this->load->view('landing/sideright.php');?>
    </div>

    <div class="menu-hider"></div>
</div>

<!-- </body> -->