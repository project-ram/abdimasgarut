<div id="page">
                
    <div class="page-content">           
        
        <div data-height="cover-header" class="caption bottom-0">
            <div class="caption-center">
                <div class="content center-text">
                    <i class="fa fa-exclamation-triangle color-green1-light fa-8x bottom-70"></i>
                    <h1 class="ultrabold fa-7x bottom-25">404</h1>
                    <h2 class="bottom-50">Page Not Found</h2>
                    <p>
                        The page you're looking for cannot be found. <br>
                    </p>
                    <a href="<?php echo base_url();?>" class="back-button button button-s bg-green1-dark button-round-small button-center-small top-50">GO BACK</a>
                </div>
            </div>
        </div>        
     
    </div>
              

	<div id="menu-contact"
         class="menu-box"
         data-menu-type="menu-box-right"
         data-menu-width="280"
         data-menu-effect="menu-reveal">
		 <?php $this->load->view('landing/sideright.php');?>
    </div>
        
  
            
    <div class="menu-hider"></div>
</div>