<?php $this->load->view('header/index');?>
<div class="preloader"><div><!--Preloader Generated Here--></div></div>

<div id="page">

	<div class="header header-fixed header-logo-app">
        <a href="#" class="back-button header-title header-subtitle"></a>
        <a href="#" class="back-button header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2" data-menu="menu-contact"><i class="fas fa-bars"></i></a>

	</div>

    <div class="page-content header-clear-medium">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/blog.css">

        <div class="content bottom-0">
			<div class="blog-categories blog-categories-1 bottom-10">
				<a href="#" class="shadow-large round-large">
                    <strong></strong>
                    <em><?php echo $cover_title;?></em>
                    <span class="bg-green1-dark opacity-50"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?><?php echo $cover_img;?>" class="preload-image responsive-image" alt="img">
                </a>
			</div>
			<!-- <div class="blog-categories blog-categories-2 bottom-10">
				<a href="#" class="shadow-large round-large">
                    <strong></strong><em>Mobile</em>
                    <span class="bg-blue2-dark opacity-70"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?>assets/images/pictures/7s.jpg" class="preload-image responsive-image" alt="img">
                </a>
				<a href="#" class="shadow-large round-large">
                    <strong></strong>
                    <em>Web</em><span class="bg-pink2-dark opacity-70"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?>assets/images/pictures/9s.jpg" class="preload-image responsive-image" alt="img">
                </a>
				<div class="clear"></div>
			</div> -->
			<div class="blog-categories blog-categories-3 bottom-10">
				<a href="<?php echo base_url();?>id/category/wisata-religi" class="shadow-large round-large">
                    <strong></strong>
                    <em>Wisata Religi</em>
                    <span class="bg-blue1-light opacity-50"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?>assets/frontend_global/img/wisata_halal/cover/wisata-religi@150.jpg" class="preload-image responsive-image" alt="img">
                </a>
				<a href="<?php echo base_url();?>id/category/masjid" class="shadow-large round-large">
                    <strong></strong>
                    <em>Masjid</em>
                    <span class="bg-red1-light opacity-40"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?>assets/frontend_global/img/wisata_halal/cover/masjid@150.jpg" class="preload-image responsive-image" alt="img">
                </a>
				<a href="<?php echo base_url();?>id/category/pesantren" class="shadow-large round-large">
                    <strong></strong>
                    <em>Pesantren</em>
                    <span class="bg-yellow1-dark opacity-40"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?>assets/frontend_global/img/wisata_halal/cover/pesantren@150.jpg" class="preload-image responsive-image" alt="img">
                </a>
				<a href="<?php echo base_url();?>id/category/tempat-penghafal-alquran" class="shadow-large round-large">
                    <strong></strong>
                    <em class="font-12 line-1">Tempat Penghafal AlQuran</em>
                    <span class="bg-pink2-dark opacity-40"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?>assets/frontend_global/img/wisata_halal/cover/tempat-penghafal-alquran@150.jpg" class="preload-image responsive-image" alt="img">
                </a>
				<a href="<?php echo base_url();?>id/category/cerita-daerah" class="shadow-large round-large">
                    <strong></strong>
                    <em>Cerita Daerah</em>
                    <span class="bg-mint-dark opacity-40"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?>assets/frontend_global/img/wisata_halal/cover/cerita-daerah@150.jpg" class="preload-image responsive-image" alt="img">
                </a>
				<a href="<?php echo base_url();?>id/category/hotel-syariah" class="shadow-large round-large">
                    <strong></strong>
                    <em>Hotel Syariah</em>
                    <span class="bg-green2-dark opacity-40"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?>assets/frontend_global/img/wisata_halal/cover/hotel-syariah@150.jpg" class="preload-image responsive-image" alt="img">
                </a>

				<div class="clear"></div>
			</div>
		</div>

        <div class="divider divider-margins"></div>
        <div class="footer text-center">
			<?php
				$this->load->view('landing/footer.php');
			?>
		</div>
    </div>
    <?php
        $this->load->view('footer_menu');
    ?>

    <div id="menu-contact"
         class="menu-box"
         data-menu-type="menu-box-right"
         data-menu-width="280"
         data-menu-load="menu-sidebar-right.html"
         data-menu-effect="menu-reveal">
    </div>

    <div id="menu-colors"
         class="menu-box round-small"
         data-menu-type="menu-box-bottom"
         data-menu-load="menu-colors.html"
         data-menu-height="400"
         data-menu-effect="menu-over">
    </div>

    <div class="menu-hider"></div>
</div>


