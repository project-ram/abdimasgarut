<?php $this->load->view('header/index');?>
<div class="preloader"><div>
<!-- Preloader Generated Here -->
</div></div>

<div id="page">
    
	<div class="header header-fixed header-logo-app">
        <a href="#" class="back-button header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2" data-menu="menu-contact"><i class="fas fa-bars"></i></a>

	</div>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/blog.css">
	<div class="mt-16">
		<div class="bottom-0">
		<div class="blog-categories blog-categories-1 bottom-10">
				<a href="#" class="round-large">
                    <strong></strong>
                    <em><?php echo $cover_title;?></em>
                    <span class="bg-green1-dark opacity-50"></span>
                    <img src="<?php echo base_url();?>assets/images/empty.png" data-src="<?php echo base_url();?><?php echo $cover_img;?>" class="preload-image responsive-image" alt="img">
                </a>
			</div>
		</div>
	</div>
                
    <div class="page-content header-clear">           
		<?php if(sizeof($post) == 0) :?>       
			<h1 class="font-20 font-bold mx-4 my-3 text-center">Oops, Belum ada post terkait <?php echo $cover_title;?></h1>
		<?php endif;?>
		<?php
			foreach($post as $item):
		?>
		<div class="blog-post-home">
			<?php 
				$def_img = 'assets/frontend_global/img/default/hotel.jpg'
			?>

				<a href="#"><img src="<?php echo base_url();?><?php echo $item->post_img != '' ? $item->post_img : $def_img; ?>" class="responsive-image"></a>
				<strong class="font-17 top-20 bolder"><?php echo $item->post_title_id;?></strong>
				<div class="blog-post-stats">
					<!-- <a href="#"><i class="fa fa-heart color-red1-light"></i>503</a> -->
					<a href="#"><i class="fa fa-eye color-dark1-light"></i><?php echo $item->post_views;?></a>
					<a href="#">  | <?php echo $item->cat_name_id;?></a>
					<div class="clear"></div>
				</div>
				<p>
					<?php y_lang_excerpt($item->post_excerpt_id, $item->post_content_id, $item->post_excerpt_en, $item->post_content_id, 150);?>
					<!-- Lorem ipsum dolor sit amet, consectetur adipi scing elit. Duis quis tempor augue.
					Phasellus faucibus urna eu tristique feugiat. -->
				</p>
				<a href="<?php echo base_url().'read/index/'.$item->post_url_id;?>-<?php echo $item->post_id;?>" class="bg-green1-dark mx-5 mt-4 text-center py-2 rounded">
					Selengkapnya
				</a>
		</div>
		<div class="divider divider-margins"></div>
		<?php endforeach;?>

		<div class="divider divider-margins"></div>

        <div class="divider divider-margins"></div>                
		<div class="footer text-center">
			<?php
				$this->load->view('landing/footer.php');
			?>
		</div>
    </div>
	<?php
        $this->load->view('footer_menu');
    ?>

	<div id="menu-contact"
         class="menu-box"
         data-menu-type="menu-box-right"
         data-menu-width="280"
         data-menu-effect="menu-reveal">
		 <?php $this->load->view('landing/sideright.php');?>
    </div>
        
  
            
    <div class="menu-hider"></div>
</div>
