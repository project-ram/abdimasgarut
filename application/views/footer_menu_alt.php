
    <?php $url = $this->uri->segment(3);?>
    <div id="footer-menu" class="px-4 footer-menu-6-icons footer-menu-style-1">
		<a href="<?php echo base_url();?>id/type/wisata-halal" 
            class="pt-2 <?php echo $url == 'wisata-halal' ? 'active':''; ?>">
            <i class="demo-icon icon-halal"></i><span>Wisata Halal</span>
        </a>
		<a href="media.html" class="pt-2"><i class="demo-icon icon-promo"></i><span>Promo</span></a>
        <a href="homepages.html" class="pt-2 <?php echo $url == '' ? 'active':''; ?>"><i class="demo-icon icon-home"></i><span>Home</span></a>
        <a href="templates.html" class ="pt-2"><i class="demo-icon icon-event"></i><span>Event</span></a>
		<a href="page-contact.html" class="pt-2"><i class="demo-icon icon-culinery"></i><span>Kuliner</span></a>
		<a href="<?php echo base_url();?>id/type/destinasi-wisata" class="pt-2 <?php echo $url == 'destinasi-wisata' ? 'color-highlight color-green1-dark':''; ?>"><i class="demo-icon icon-destination"></i><span>Destinasi Wisata</span></a>
        <div class="clear"></div>
    </div>

