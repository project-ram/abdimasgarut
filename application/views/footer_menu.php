
    <?php 
        $url = $this->uri->segment(3);
        $home = $this->uri->segment(1);
    ?>
    <div id="footer-menu" class="px-4 footer-menu-6-icons footer-menu-style-1">
        <a href="<?php echo base_url();?>" class="pt-5 <?php echo ($url == '' && $home == '') ? 'active':''; ?>"><i class="demo-icon icon-home"></i></a>
		<a href="<?php echo base_url();?>id/type/wisata-halal" 
            class="pt-5 <?php echo $url == 'wisata-halal' ? 'active':''; ?>">
            <i class="demo-icon icon-halal"></i>
        </a>
		<a href="<?php echo base_url();?>id/type/promo" class="pt-5 <?php echo $url == 'promo' ? 'active':''; ?>"><i class="demo-icon icon-promo"></i></a>
        <a href="<?php echo base_url();?>id/type/event" class ="pt-5 <?php echo $url == 'event' ? 'active':''; ?>"><i class="demo-icon icon-event"></i></a>
		<a href="<?php echo base_url();?>id/type/kuliner" class="pt-5  <?php echo $url == 'kuliner' ? 'active':''; ?>"><i class="demo-icon icon-culinery"></i></a>
		<a href="<?php echo base_url();?>id/type/destinasi-wisata" class="pt-5 <?php echo $url == 'destinasi-wisata' ? 'color-highlight color-green1-dark':''; ?>"><i class="demo-icon icon-destination"></i></a>
        <div class="clear"></div>
    </div>

