<?php $this->load->view('header/index');?>
<div id="page">
    
	<div class="header header-fixed header-logo-app">
        <a href="#" class="back-button header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2" data-menu="menu-contact"><i class="fas fa-bars"></i></a>

	</div>

                
    <div class="page-content header-clear-medium">            
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/news.css">
        <div data-height="180" class="caption caption-margins round-medium shadow-large">
            <div class="caption-bottom left-20 right-20">
                <h1 class="color-white bolder font-16 line-1"><?php echo $data->post_title_id;?></h1>
                <div class="one-half">
                    <p class="text-left text-white font-10"><i class="fa fa-user right-10"></i> Admin </p>
                </div>
                <div class="one-half last-column">
                    <p class="text-right text-white font-10">
                        <?php echo date_format(date_create($data->post_date),"d/m/Y");?>
                    <i class="fa fa-clock left-10"></i></p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="caption-overlay bg-black opacity-50"></div>
            <div class="caption-bg">
				<?php $def_img = "assets/frontend_global/img/default/no-img-default.jpg";?>
                <img src="<?php echo base_url();?><?php echo $data->post_img != '' ? $data->post_img : $def_img ;?>" class="responsive-image">
            </div>
        </div>    

		<div class="content reading-time-box reading-words">
  			<p>
                <?php echo $data->post_content_id;?>  
            </p>
            <div class="divider"></div>
            <!-- Tags -->
            <h5 class="font-bold mt-2 mb-2 font-16">Tags</h5>
             <?php 
                if(sizeof($tags) != 0):
                    foreach($tags as $item):
             ?>
                <div class="mb-4 inline-block rounded mr-3  
                    <?php echo $item->tag_name != '' ? "bg-green1-dark text-center" :"text-black";?>
                    <?php 
                        $str = explode(" ", $item->tag_name);
                        $len = sizeof($str);

                        echo $len > 1 ? "w-2/4":" w-1/4";
                    ?>" 
                >
                   <?php echo $item->tag_name != '' ? $item->tag_name :"-";?>
                </div>
            <?php 
                endforeach;
            else:
                echo "<p class='font-16 ml-1'>-</p>";
            endif; ?>
            <div class="divider"></div>
            <h5 class="font-bold">Bagikan ke sosial media mu:</h5>
			<div class="social-icons top-30 bottom-15">
				<a href="#" class="icon icon-xs icon-round shadow-large bg-facebook"><i class="fab fa-facebook-f"></i></a>
				<a href="#" class="icon icon-xs icon-round shadow-large bg-twitter"><i class="fab fa-twitter"></i></a>
				<a href="#" class="icon icon-xs icon-round shadow-large bg-google"><i class="fab fa-google-plus-g"></i></a>
				<a href="#" class="icon icon-xs icon-round shadow-large bg-pinterest"><i class="fab fa-pinterest-p"></i></a>
			</div>
          
			<div class="divider"></div>
			<!-- <h3 class="bolder">This is an Image Gallery</h3>
			<p>
				This page is not WordPress, it's an HTML website. Yes, that's right. but you can convert it to WordPress yourself
				or hire an expert from Envato Studio.
			</p>
			<div class="gallery">
			<a href="images/pictures/6.jpg" class="show-gallery polaroid-effect round-small bottom-30" title="Beautiful Camera">
				<img src="images/empty.png" data-src="images/pictures/6w.jpg" class="preload-image round-small responsive-image bottom-0" alt="img">
				<p class="bottom-0 top-5 font-10 center-text">Polaroid Effect Image</p>
			</a>
			</div>
			<div class="one-half">
				<h4 class="bolder bottom-10">1/1</h4>
				<img data-src="images/pictures/isolated/1.jpg" src="images/empty.png" class="preload-image round-small shadow-large responsive-image" alt="img">
				<p>This is half a column.</p>
			</div>
			<div class="one-half last-column">
				<h4 class="bolder bottom-10">1/2</h4>
				<img data-src="images/pictures/isolated/2.jpg" src="images/empty.png" class="preload-image round-small shadow-large responsive-image" alt="img">
				<p>This is half a column.</p>
			</div>
			<div class="clear"></div>

			<div class="divider"></div>

			<div class="one-third">
				<h4 class="bolder bottom-20">1/3</h4>
				<img data-src="images/pictures/isolated/3.jpg" src="images/empty.png" class="preload-image round-small shadow-large responsive-image" alt="img">
				<p>This is a third of a column.</p>
			</div>
			<div class="one-third">
				<h4 class="bolder bottom-20">2/3</h4>
				<img data-src="images/pictures/isolated/4.jpg" src="images/empty.png" class="preload-image round-small shadow-large responsive-image" alt="img">
				<p>This is a third of a column.</p>
			</div>
			<div class="one-third last-column">
				<h4 class="bolder bottom-20">2/3</h4>
				<img data-src="images/pictures/isolated/5.jpg" src="images/empty.png" class="preload-image round-small shadow-large responsive-image" alt="img">
				<p>This is a third of a column.</p>
			</div>
			<div class="clear"></div>
			<div class="divider"></div>
			<h4 class="bolder">Lists</h4>
			<p>All sorts of lists,  even the feature page is a list, but classic ones are also necessary an included in our item.</p>
			<ul class="one-half">
				<li>Unordered List</li>
				<li>Unordered List
					<ul>
						<li>Nested Sublist</li>
						<li>Nested Sublist</li>
						<li>Nested Sublist</li>
					</ul>
				</li>
				<li>Unordered List</li>
				<li>Unordered List</li>
			</ul>
			<ol class="one-half last-column bottom-30">
				<li>Ordered List</li>
				<li>Ordered List
					<ol>
						<li>Nested Sublist</li>
						<li>Nested Sublist</li>
						<li>Nested Sublist</li>
					</ol>
				</li>
				<li>Ordered List</li>
				<li>Ordered List</li>
			</ol>
			<div class="clear"></div>
			<div class="divider"></div>
			<a href="#" class="back-button button button-s button-full button-round-medium bg-highlight shadow-large">Back to News</a>
		</div> -->
    </div>
        			        
    <div class="footer text-center">
			<?php
				$this->load->view('landing/footer')
			?>
		</div>   
	<?php
		$this->load->view('footer_menu')
	?>
           
    <div id="menu-contact"
         class="menu-box"
         data-menu-type="menu-box-right"
         data-menu-width="280"
         data-menu-effect="menu-reveal">
		 <?php include('sideright.php');?>
    </div>

            
    <div class="menu-hider"></div>
</div>