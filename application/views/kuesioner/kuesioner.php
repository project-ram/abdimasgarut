<?php $this->load->view('header/index');?>
<div class="preloader"><div>
</div></div>

<div id="page" class="h-screen">
    <div class="header header-fixed header-logo-app">
        <a href="#" class="back-button header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2" data-menu="menu-contact"><i class="fas fa-bars"></i></a>

	</div>
    <div id="step0" class="mt-20 mx-5">
        <div class="text-2xl font-bold text-center leading-tight">Kuesioner <br> <span class="italic">Customer Satisfaction Index</span> </div>
        <p class="mt-4">Kuesioner terbagi atas 2 bagian dengan pernyataan yang sama.</p>
        <ol class="mt-5 mx-5 list-decimal">
            <li>Bagian pertama digunakan untuk mengukur tingkat kepentingan dari setiap atribut.</li>
            <li>Bagian kedua digunakan untuk mengukur tingkat kepuasan atau kinerja dari setiap atribut</li>
        </ol>
        <div class="text-center mt-5">
            <button class="button button-3d button-m button-round-small border-green1-dark bg-green1-light" onclick="start()">Isi Kuesioner</button>
        </div>
    </div>
    <form method="post" action="<?php echo base_url();?>Kuesioner/submitForm">
        <div id="step1" class="mx-5 mt-20">
            <p class="font-20 mb-4 bg-green1-dark px-3 py-3">Bagian 1: Tingkat Kepentingan</p>
            <!-- 1. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website menyajikan informasi yang sesuai dengan kebutuhan.<span class="color-red1-dark ">*</span></label>
                <div class="fac fac-radio-full fac-green block"><span></span>
                    <input id="info_web1-fac-radio-full" type="radio" name="info_kebutuhan" value="1">
                    <label for="info_web1-fac-radio-full">Sangat Tidak Penting</label>
                </div>
                <div class="fac fac-radio-full fac-green block"><span></span>
                    <input id="info_web2-fac-radio-full" type="radio" name="info_kebutuhan" value="2">
                    <label for="info_web2-fac-radio-full">Tidak Penting</label>
                </div>
                <div class="fac fac-radio-full fac-green block"><span></span>
                    <input id="info_web3-fac-radio-full" type="radio" name="info_kebutuhan" value="3">
                    <label for="info_web3-fac-radio-full">Penting</label>
                </div>
                <div class="fac fac-radio-full fac-green block"><span></span>
                    <input id="info_web4-fac-radio-full" type="radio" name="info_kebutuhan" value="4">
                    <label for="info_web4-fac-radio-full">Sangat Penting</label>
                </div>
            </div>
            <!-- 2. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website menyediakan fasilitas yang memungkinkan mencari informasi yang dibutuhkan.<span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="fasilitas_web1-fac-radio-full" type="radio" name="fasilitas_web" value="1">
                        <label for="fasilitas_web1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="fasilitas_web2-fac-radio-full" type="radio" name="fasilitas_web" value="2">
                        <label for="fasilitas_web2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="fasilitas_web3-fac-radio-full" type="radio" name="fasilitas_web" value="3">
                        <label for="fasilitas_web3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="fasilitas_web4-fac-radio-full" type="radio" name="fasilitas_web" value="4">
                        <label for="fasilitas_web4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <!-- 3. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website dapat diakses dengan cepat. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="akses_web1-fac-radio-full" type="radio" name="akses_web" value="1">
                        <label for="akses_web1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="akses_web2-fac-radio-full" type="radio" name="akses_web" value="2">
                        <label for="akses_web2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="akses_web3-fac-radio-full" type="radio" name="akses_web" value="3">
                        <label for="akses_web3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="akses_web4-fac-radio-full" type="radio" name="akses_web" value="4">
                        <label for="akses_web4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <!-- 4. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website menyajikan informasi yang mudah untuk dipahami. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="paham1-fac-radio-full" type="radio" name="paham" value="1">
                        <label for="paham1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="paham2-fac-radio-full" type="radio" name="paham" value="2">
                        <label for="paham2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="paham3-fac-radio-full" type="radio" name="paham" value="3">
                        <label for="paham3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="paham4-fac-radio-full" type="radio" name="paham" value="4">
                        <label for="paham4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <!-- 5. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website mudah untuk digunakan. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="mudah1-fac-radio-full" type="radio" name="mudah" value="1">
                        <label for="mudah1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="mudah2-fac-radio-full" type="radio" name="mudah" value="2">
                        <label for="mudah2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="mudah3-fac-radio-full" type="radio" name="mudah" value="3">
                        <label for="mudah3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="mudah4-fac-radio-full" type="radio" name="mudah" value="4">
                        <label for="mudah4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <!-- 6. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website memiliki desain yang menarik. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="desain1-fac-radio-full" type="radio" name="desain" value="1">
                        <label for="desain1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="desain2-fac-radio-full" type="radio" name="desain" value="2">
                        <label for="desain2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="desain3-fac-radio-full" type="radio" name="desain" value="3">
                        <label for="desain3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="desain4-fac-radio-full" type="radio" name="desain" value="4">
                        <label for="desain4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <!-- 7. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website memiliki desain yang inovatif. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="inovatif1-fac-radio-full" type="radio" name="inovatif" value="1">
                        <label for="inovatif1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="inovatif2-fac-radio-full" type="radio" name="inovatif" value="2">
                        <label for="inovatif2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="inovatif3-fac-radio-full" type="radio" name="inovatif" value="3">
                        <label for="inovatif3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="inovatif4-fac-radio-full" type="radio" name="inovatif" value="4">
                        <label for="inovatif4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <!-- 8. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website menyenangkan untuk digunakan.<span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="menyenangkan1-fac-radio-full" type="radio" name="menyenangkan" value="1">
                        <label for="menyenangkan1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="menyenangkan2-fac-radio-full" type="radio" name="menyenangkan" value="2">
                        <label for="menyenangkan2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="menyenangkan3-fac-radio-full" type="radio" name="menyenangkan" value="3">
                        <label for="menyenangkan3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="menyenangkan4-fac-radio-full" type="radio" name="menyenangkan" value="4">
                        <label for="menyenangkan4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <!-- 9. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website sesuai dengan profil pariwisata Kabupaten Garut. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="profil_garut1-fac-radio-full" type="radio" name="profil_garut" value="1">
                        <label for="profil_garut1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="profil_garut2-fac-radio-full" type="radio" name="profil_garut" value="2">
                        <label for="profil_garut2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="profil_garut3-fac-radio-full" type="radio" name="profil_garut" value="3">
                        <label for="profil_garut3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="profil_garut4-fac-radio-full" type="radio" name="profil_garut" value="4">
                        <label for="profil_garut4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <!-- 10. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website lebih memudahkan urusan dalam mencari informasi pariwisata di Kabupaten Garut dibandingkan dengan media lain.<span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="search_info1-fac-radio-full" type="radio" name="search_info" value="1">
                        <label for="search_info1-fac-radio-full">Sangat Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="search_info2-fac-radio-full" type="radio" name="search_info" value="2">
                        <label for="search_info2-fac-radio-full">Tidak Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="search_info3-fac-radio-full" type="radio" name="search_info" value="3">
                        <label for="search_info3-fac-radio-full">Penting</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="search_info4-fac-radio-full" type="radio" name="search_info" value="4">
                        <label for="search_info4-fac-radio-full">Sangat Penting</label>
                    </div>
            </div>
            <div class="text-center mt-12 mb-24">
                <button type="button" class="button button-m button-round-small bg-green1-dark w-8/12" onclick="next()">Selanjutnya</button>
            </div>
        </div>
        <div id="step2" class="mx-5 mt-20">
            <p class="font-20 mb-4 bg-green1-dark px-3 py-3">Bagian 2: Tingkat Kepuasan</p>
            <!-- 1. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website menyajikan informasi yang sesuai dengan kebutuhan.<span class="color-red1-dark ">*</span></label>
                <div class="fac fac-radio-full fac-green block"><span></span>
                    <input id="info_web21-fac-radio-full" type="radio" name="info_kebutuhan" value="1">
                    <label for="info_web21-fac-radio-full">Sangat Tidak Puas</label>
                </div>
                <div class="fac fac-radio-full fac-green block"><span></span>
                    <input id="info_web22-fac-radio-full" type="radio" name="info_kebutuhan" value="2">
                    <label for="info_web22-fac-radio-full">Tidak Puas</label>
                </div>
                <div class="fac fac-radio-full fac-green block"><span></span>
                    <input id="info_web23-fac-radio-full" type="radio" name="info_kebutuhan" value="3">
                    <label for="info_web23-fac-radio-full">Puas</label>
                </div>
                <div class="fac fac-radio-full fac-green block"><span></span>
                    <input id="info_web24-fac-radio-full" type="radio" name="info_kebutuhan" value="4">
                    <label for="info_web24-fac-radio-full">Sangat Puas</label>
                </div>
            </div>
            <!-- 2. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website menyediakan fasilitas yang memungkinkan mencari informasi yang dibutuhkan.<span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="fasilitas_web21-fac-radio-full" type="radio" name="fasilitas_web2" value="1">
                        <label for="fasilitas_web21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="fasilitas_web22-fac-radio-full" type="radio" name="fasilitas_web2" value="2">
                        <label for="fasilitas_web22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="fasilitas_web23-fac-radio-full" type="radio" name="fasilitas_web2" value="3">
                        <label for="fasilitas_web23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="fasilitas_web24-fac-radio-full" type="radio" name="fasilitas_web2" value="4">
                        <label for="fasilitas_web24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <!-- 3. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website dapat diakses dengan cepat. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="akses_web21-fac-radio-full" type="radio" name="akses_web2" value="1">
                        <label for="akses_web21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="akses_web22-fac-radio-full" type="radio" name="akses_web2" value="2">
                        <label for="akses_web22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="akses_web23-fac-radio-full" type="radio" name="akses_web2" value="3">
                        <label for="akses_web23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="akses_web24-fac-radio-full" type="radio" name="akses_web2" value="4">
                        <label for="akses_web24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <!-- 4. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website menyajikan informasi yang mudah untuk dipahami. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="paham21-fac-radio-full" type="radio" name="paham2" value="1">
                        <label for="paham21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="paham22-fac-radio-full" type="radio" name="paham2" value="2">
                        <label for="paham22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="paham23-fac-radio-full" type="radio" name="paham2" value="3">
                        <label for="paham23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="paham24-fac-radio-full" type="radio" name="paham2" value="4">
                        <label for="paham24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <!-- 5. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website mudah untuk digunakan. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="mudah21-fac-radio-full" type="radio" name="mudah2" value="1">
                        <label for="mudah21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="mudah22-fac-radio-full" type="radio" name="mudah2" value="2">
                        <label for="mudah22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="mudah23-fac-radio-full" type="radio" name="mudah2" value="3">
                        <label for="mudah23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="mudah24-fac-radio-full" type="radio" name="mudah2" value="4">
                        <label for="mudah24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <!-- 6. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website memiliki desain yang menarik. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="desain21-fac-radio-full" type="radio" name="desain2" value="1">
                        <label for="desain21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="desain22-fac-radio-full" type="radio" name="desain2" value="2">
                        <label for="desain22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="desain23-fac-radio-full" type="radio" name="desain2" value="3">
                        <label for="desain23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="desain24-fac-radio-full" type="radio" name="desain2" value="4">
                        <label for="desain24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <!-- 7. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website memiliki desain yang inovatif. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="inovatif21-fac-radio-full" type="radio" name="inovatif2" value="1">
                        <label for="inovatif21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="inovatif22-fac-radio-full" type="radio" name="inovatif2" value="2">
                        <label for="inovatif22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="inovatif23-fac-radio-full" type="radio" name="inovatif2" value="3">
                        <label for="inovatif23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="inovatif24-fac-radio-full" type="radio" name="inovatif2" value="4">
                        <label for="inovatif24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <!-- 8. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website menyenangkan untuk digunakan.<span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="menyenangkan21-fac-radio-full" type="radio" name="menyenangkan2" value="1">
                        <label for="menyenangkan21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="menyenangkan22-fac-radio-full" type="radio" name="menyenangkan2" value="2">
                        <label for="menyenangkan22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="menyenangkan23-fac-radio-full" type="radio" name="menyenangkan2" value="3">
                        <label for="menyenangkan23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="menyenangkan24-fac-radio-full" type="radio" name="menyenangkan2" value="4">
                        <label for="menyenangkan24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <!-- 9. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website sesuai dengan profil pariwisata Kabupaten Garut. <span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="profil_garut21-fac-radio-full" type="radio" name="profil_garut2" value="1">
                        <label for="profil_garut21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="profil_garut22-fac-radio-full" type="radio" name="profil_garut2" value="2">
                        <label for="profil_garut22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="profil_garut23-fac-radio-full" type="radio" name="profil_garut2" value="3">
                        <label for="profil_garut23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="profil_garut24-fac-radio-full" type="radio" name="profil_garut2" value="4">
                        <label for="profil_garut24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <!-- 10. -->
            <div class="mb-5 h-22">
                <label class="font-bold">Website lebih memudah2kan urusan dalam mencari informasi pariwisata di Kabupaten Garut dibandingkan dengan media lain.<span class="color-red1-dark ">*</span></label>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="search_info21-fac-radio-full" type="radio" name="search_info2" value="1">
                        <label for="search_info21-fac-radio-full">Sangat Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="search_info22-fac-radio-full" type="radio" name="search_info2" value="2">
                        <label for="search_info22-fac-radio-full">Tidak Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="search_info23-fac-radio-full" type="radio" name="search_info2" value="3">
                        <label for="search_info23-fac-radio-full">Puas</label>
                    </div>
                    <div class="fac fac-radio-full fac-green block"><span></span>
                        <input id="search_info24-fac-radio-full" type="radio" name="search_info2" value="4">
                        <label for="search_info24-fac-radio-full">Sangat Puas</label>
                    </div>
            </div>
            <div class="text-center mt-12 mb-24 justify-between flex ">
                <button type="button" class="button button-m button-round-small bg-green1-dark" onclick="prev()"><i class="fas fa-arrow-left"></i> Kembali</button>
                <button type="submit" class="button button-m button-round-small bg-green1-dark">Kirim</button>
            </div>
        </div>        
    </form>
    <?php
       $this->load->view('footer_menu')
    ?>   
</div>
<!-- <script src="https://code.jquery.com/jquery-3.4.1.js"></script> -->
<script>
    $(document).ready(function() {
        $('#step1').hide();
        $('#step2').hide();
    });
    function start(){
        $('#step0').hide();
        $('#step1').fadeIn();
        $('#step2').hide();
    }
    function next(){
        $('#step0').hide();
        $('#step1').fadeOut();
        $('#step2').fadeIn();
    }
    function prev(){
        $('#step0').hide();
        $('#step1').fadeIn();
        $('#step2').fadeOut();
    }
</script>