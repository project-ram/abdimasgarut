<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-capable" content="yes">
<title>DuoDrawer</title>
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="<?php echo base_url(); ?>assets/_manifest.json" data-pwa-version="set_by_pwa.js">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/framework.css">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/css/fontawesome-all.min.css">
<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
<link rel="stylesheet" href='<?php echo base_url(); ?>assets/fontello/css/fontello.css'>
<link rel="stylesheet" href='<?php echo base_url(); ?>assets/fontello/css/fontello-codes.css'>
</head>
    
<body class="theme-light" data-highlight="blue2">

    <?php echo $content; ?>

    <script src="<?php echo base_url(); ?>assets/scripts/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/scripts/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/scripts/custom.js"></script>
</body>


