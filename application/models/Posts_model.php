<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Posts_model extends CI_Model
{
    private $table = 'posts';
    private $id = 'post_id';

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    public function get_slider()
    {
        //
        return $this->db->query("SELECT post_id, post_url_id, post_url_en, post_date, post_views, post_content_id, post_title_id, post_excerpt_id, post_content_en, post_title_en, post_excerpt_en, post_img, cat_id, cat_name_id, cat_name_en, cat_url_id, cat_url_en, post_address, post_lat, post_long, post_type
								 FROM posts_cat
								 LEFT JOIN " . $this->table . " ON pc_id_post = post_id
								 LEFT JOIN ms_category ON pc_id_cat = cat_id
								 WHERE post_slider = '1'
								 GROUP BY post_id
								 ORDER BY post_date DESC
								 LIMIT 0,5");
    }
    public function get_popular()
    {
        return $this->db->query("SELECT post_id, post_url_id, post_url_en, post_date, post_views, post_content_id, post_title_id, post_excerpt_id, post_content_en, post_title_en, post_excerpt_en, post_img, cat_id, cat_name_id, cat_name_en, cat_url_id, cat_url_en, post_address, post_lat, post_long, post_type
								 FROM posts_cat
								 LEFT JOIN " . $this->table . " ON pc_id_post = post_id
								 LEFT JOIN ms_category ON pc_id_cat = cat_id
								 WHERE cat_type = 'destinasi-wisata'
								 GROUP BY post_id
								 ORDER BY post_views DESC, post_date DESC
								 LIMIT 0,4");
    }
    public function get_post_cat($cat)
    {
        return $this->db->query("SELECT post_id, post_url_id, post_url_en, post_date, post_views, post_content_id, post_title_id, post_excerpt_id, post_content_en, post_title_en, post_excerpt_en, post_img,
									cat_id, cat_name_id, cat_name_en, cat_url_id, cat_url_en, post_address, post_lat, post_long, post_type
								 FROM  " . $this->table . "
								 LEFT JOIN posts_cat ON post_id = pc_id_post
								 LEFT JOIN ms_category ON pc_id_cat = cat_id
								 WHERE cat_type = '$cat'
								 GROUP BY post_id
								 ORDER BY post_date DESC, post_views DESC");

    }
    public function get_post_promo_landing()
    {
        return $this->db->query("SELECT post_id, post_url_id, post_url_en, post_date, post_views, post_content_id, post_title_id, post_excerpt_id, post_content_en, post_title_en, post_excerpt_en, post_img,
									cat_id, cat_name_id, cat_name_en, cat_url_id, cat_url_en, post_address, post_lat, post_long, post_type
								 FROM  " . $this->table . "
								 LEFT JOIN posts_cat ON post_id = pc_id_post
								 LEFT JOIN ms_category ON pc_id_cat = cat_id
								 WHERE cat_type = 'event'
								 GROUP BY post_id
								 ORDER BY post_date DESC, post_views DESC
								 LIMIT 0,4");

    }
    public function getbyid($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table);
    }
    public function get_post_cat_submenu($cat,$cat_name)
    {
        return $this->db->query(
		"SELECT post_id,
					post_url_id,
					post_url_en,
					post_date,
					post_views,
					post_content_id,
					post_title_id,
					post_excerpt_id,
					post_content_en,
					post_title_en,
					post_excerpt_en,
					post_img,
					cat_id,
					cat_name_id,
					cat_name_en,
					cat_url_id,
					cat_url_en,
					post_address,
					post_lat,
					post_long,
					post_type
				FROM posts
						LEFT JOIN posts_cat ON post_id = pc_id_post
						LEFT JOIN ms_category ON pc_id_cat = cat_id
				WHERE cat_type = '$cat' AND cat_name_id = '$cat_name'
				GROUP BY post_id
				ORDER BY post_date DESC, post_views DESC;"

        );
    }
    // ------------------------------------------------------------------------

}

/* End of file Posts_model.php */
/* Location: ./application/models/Posts_model.php */
