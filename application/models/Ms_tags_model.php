<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * Model Ms_tags_model
 *
 * This Model for ...
 * 
 * @package		CodeIgniter
 * @category	Model
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Ms_tags_model extends CI_Model {

  // ------------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();
  }

  // ------------------------------------------------------------------------


  // ------------------------------------------------------------------------
  function get_tag_bypost($id)
	{
		return $this->db->query("SELECT * FROM posts_tag 
								 LEFT JOIN ms_tags ON pt_id_tag = tag_id
								 WHERE pt_id_post = '$id'");
	}

  // ------------------------------------------------------------------------

}

/* End of file Ms_tags_model.php */
/* Location: ./application/models/Ms_tags_model.php */