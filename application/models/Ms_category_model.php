<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_category_model extends CI_Model {

  // ------------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();
  }

  // ------------------------------------------------------------------------


  // ------------------------------------------------------------------------
  public function all()
  {
    // 
    return $this->db->query("SELECT * FROM ms_category");
  }

  // ------------------------------------------------------------------------

}

/* End of file Ms_category_model.php */
/* Location: ./application/models/Ms_category_model.php */