<?php

    Class Template_Controller extends CI_Controller{
        function tmp_header($content, $data = NULL){
            $data['content'] = $this->load->view($content, $data,TRUE);
            $this->load->view('header/index',$data,FALSE);

        }
        function cat_header($content, $data = NULL){
            $data['content'] = $this->load->view($content, $data,TRUE);
            $this->load->view('header/category',$data,FALSE);

        }
    }