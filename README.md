## Visit Garut PWA Version

**Proyek ini merupakan kerjasama Fakultas Informatika Telkom University dengan dinas pariwisata Kabupaten Garut, Jawa Barat**

### Cara Menjalankan

**NOTE: Pastikan PC Anda telah terinstal** 
1.  git, jika belum unduh [disini](https://git-scm.com/)
2. XAMPP versi terbaru

- Buka Folder `C:/xampp/htdocs/` kemudian arahkan git bash ke folder ini, lalu jalankan perintah
 ```
$ git clone https://gitlab.com/project-ram/abdimasgarut.git
```
- Setelah selesai, buka folder `abdimasgarut`, lalu masuk ke dalam `application/config/config.php`
- Ganti isi ``$config['base_url']`` menjadi `http://localhost/abdimasgarut/`
-  Nyalakan apache XAMPP
 - pada browser ketik  `http://localhost/abdimasgarut/`
